FROM python:3.10-slim-bullseye

# Create a working directory
RUN mkdir /app
WORKDIR /app

# Copy the requirements file
COPY requirements.txt .

# Install the packages in the requirements file
RUN python3 -m pip install -r requirements.txt

# enable nbextension widgets
RUN jupyter contrib nbextension install --user

# Create volumes for the Jupyter Notebook files and data files
VOLUME /app/workspace

# Start Jupyter Notebook
CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--notebook-dir=/app/workspace"]
