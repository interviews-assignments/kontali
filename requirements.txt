jupyter==1.0.0
jupyter_contrib_nbextensions==0.7.0
matplotlib==3.6.2
pandas==1.5.2
openpyxl==3.0.10
scipy==1.10.0