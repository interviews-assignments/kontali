Python 3.10 Jupyter Notebook Docker Image
=========================================

This repository contains a Docker image for running a Jupyter Notebook with Python 3.10.

Usage
-----

To use this image, you will need to have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed on your machine. Optionally, you can use [Task](https://taskfile.dev/) to run docker-compose commands. 

### 1. Build the Docker image and start the Jupyter Notebook using docker-compose:

    docker-compose up


### 2. Build the Docker image and start the Jupyter Notebook using the taskfile.yml:

    task up

### 3. Alternatively, you can use the docker build and docker run commands to start the Jupyter Notebook:

    docker build -t my-image .

    docker run -p 8888:8888 --name kontali-assignment -v $(pwd)/workspace:/app/workspace my-image

Either approach will build the Docker image, start the Jupyter Notebook, and expose it on port 8888. You should then be able to access the Jupyter Notebook at [http://localhost:8888](http://localhost:8888/) in your web browser with the token available in your terminal. They will also mount the /app/workspace to the workspace directory on your local machine. You should then be able to access the Jupyter Notebook at [http://localhost:8888](http://localhost:8888/) in your web browser.


Assignment 2
-----
Run the jupyter notebooks in the `notebooks` folder for answers to assignment 2.